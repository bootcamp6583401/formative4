import java.util.ArrayList;

/**
 * Playlist
 */
public class Playlist {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = songs;
    }

    private ArrayList<Song> songs;

    public Playlist(String title) {
        this.title = title;
        this.songs = new ArrayList<>();
    }

    public void listSongs() {
        for (int i = 0; i < songs.size(); i++) {
            System.out.println(String.format("%d. %s", i + 1, songs.get(i)));
        }
    }

    public void addSong(Song song) {
        this.songs.add(song);
    }

    public void display() {
        System.out.println(String.format("===PLAYLIST %s===\n", title));
        listSongs();
    }

}