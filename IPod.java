import java.util.ArrayList;
import java.util.Scanner;
import java.util.Comparator;
import java.util.List;

public class IPod {
    private ArrayList<Song> songs;
    private ArrayList<Playlist> playlists;
    private Scanner scanner;
    private String username;

    public IPod() {
        songs = new ArrayList<>();
        playlists = new ArrayList<>();
        scanner = new Scanner(System.in);
        System.out.print("Set username: ");
        this.username = scanner.nextLine();
    }

    public void run() {
        int answer;

        do {
            System.out.println(String.format("=== %s's iPod ===", this.username));
            System.out.println("1. List Songs");
            System.out.println("2. Add Song");
            System.out.println("3. List Playlist");
            System.out.println("4. Add Playlist");
            System.out.println("5. List Reviews");
            System.out.println("6. Review Song");
            System.out.println("7. List Top 3");
            System.out.println("8. List Bottom 3");
            System.out.println("9. Update Username");
            System.out.println("10. Exit.");
            System.out.print("Pilih menu (1-10): ");
            answer = Integer.parseInt(scanner.nextLine());
            switch (answer) {
                case 1:
                    listSongs();
                    break;
                case 2:
                    addSong();
                    break;
                case 3:
                    listPlaylists();
                    break;
                case 4:
                    addPlaylist();
                    break;
                case 5:
                    listReviews();
                    break;
                case 6:
                    review();
                    break;
                case 7:
                    listTop3();
                    break;
                case 8:
                    listBottom3();
                    break;
                case 9:
                    updateUsername();
                    break;
                case 10:
                    System.out.println("Exit");
                    break;
                default:
                    System.out.println("Invalid");
                    break;
            }
        } while (answer != 10);
    }

    public void listSongs() {
        System.out.println("===== YOUR SONGS =====");
        for (int i = 0; i < songs.size(); i++) {
            System.out.println(String.format("%d. %s - %s", i + 1, songs.get(i).getTitle(), songs.get(i).getArtist()));
        }
    }

    public void addSong() {
        System.out.print("Song name: ");
        String title = scanner.nextLine();

        System.out.print("Song artist: ");
        String artist = scanner.nextLine();

        System.out.print("Genre: ");
        String genre = scanner.nextLine();

        this.songs.add(new Song("1", title, genre, artist));

        System.out.println("Song added!");
    }

    public void listPlaylists() {
        System.out.println("===== YOUR PLAYLISTS =====");
        for (int i = 0; i < playlists.size(); i++) {
            System.out.println(String.format("%d. %s (%d songs)", i + 1, playlists.get(i).getTitle(),
                    playlists.get(i).getSongs().size()));
        }
    }

    public void addPlaylist() {
        if (songs.size() <= 0) {
            System.out.println("No songs in your library.");
        } else {
            System.out.println("Playlist Title: ");
            String title = scanner.nextLine();

            Playlist newPlaylist = new Playlist(title);

            listSongs();
            System.out.println("Choose songs to include: (seperate with space)");
            String indexString = scanner.nextLine();

            String[] indexes = indexString.split(" ");

            int addedSongs = 0;

            for (String index : indexes) {
                int intIndex = Integer.parseInt(index);

                if (intIndex <= songs.size() && intIndex >= 0) {
                    newPlaylist.addSong(songs.get(intIndex - 1));
                    addedSongs++;
                }
            }

            this.playlists.add(newPlaylist);

            System.out.println(String.format("Playlist created. Added %d songs", addedSongs));
        }

    }

    public void review() {
        listSongs();
        System.out.print("Choose song to review: ");
        int index = Integer.parseInt(scanner.nextLine());
        if ((index - 1) >= 0 && (index - 1) < songs.size()) {
            Song song = songs.get(index - 1);
            System.out.println("Song: " + song);
            System.out.print("Give score (0-100): ");
            int score = Integer.parseInt(scanner.nextLine());
            song.review(score);
        }
    }

    public void listReviews() {
        for (int i = 0; i < songs.size(); i++) {
            System.out.println(String.format("%d. %s: %d/100", i + 1, songs.get(i),
                    songs.get(i).isReviewed() ? songs.get(i).getScore() : "No Reviewd"));
        }
    }

    public void listTop3() {
        songs.sort(Comparator.comparingInt(Song::getScore).reversed());

        List<Song> bottom3 = songs.subList(0, Math.min(3, songs.size()));

        System.out.println("===== TOP 3 SONGS =====");
        for (int i = 0; i < bottom3.size(); i++) {
            System.out.println(
                    String.format("%d. %s - %s", i + 1, bottom3.get(i).getTitle(), bottom3.get(i).getArtist()));
        }

    }

    public void listBottom3() {
        songs.sort(Comparator.comparingInt(Song::getScore));

        List<Song> topThree = songs.subList(0, Math.min(3, songs.size()));

        System.out.println("===== BOTTOM 3 SONGS =====");
        for (int i = 0; i < topThree.size(); i++) {
            System.out.println(
                    String.format("%d. %s - %s", i + 1, topThree.get(i).getTitle(), topThree.get(i).getArtist()));
        }
    }

    public void updateUsername() {
        System.out.print("New username: ");
        String newUsername = scanner.nextLine();
        this.username = newUsername;
        System.out.println("New username is " + newUsername);
    }

}
