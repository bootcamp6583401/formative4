/**
 * Song
 */
public class Song {

    private String id;
    private String title;
    private String genre;
    private String artist;

    private int score = 0;
    private boolean isReviewed = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getScore() {
        return score;
    }

    public boolean isReviewed() {
        return isReviewed;
    }

    public Song(String id, String title, String genre, String artist) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.artist = artist;
    }

    public void review(int score) {
        if (score >= 0 && score <= 100) {
            this.score = score;
            this.isReviewed = true;
        }
    }

    @Override
    public String toString() {
        return String.format("%s - %s", title, artist);
    }

}